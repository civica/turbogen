package Turbogen::TimeService;

use POSIX;

sub get_current_timestamp {

    return POSIX::strftime('%Y%m%d-%H%M%S', localtime());
}

return 1;
