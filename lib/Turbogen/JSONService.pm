package Turbogen::JSONService;

use JSON::MaybeXS;

sub decode_json_to_hash {

    my $json = shift;

    return JSON::MaybeXS::decode_json($json);
}

sub encode_hash_to_json {

    my $hash = shift;

    my $encoder = JSON::MaybeXS->new(utf8 => 1, pretty => 1, sort_by => 1);

    return $encoder->encode($hash);
}

return 1;
