package Turbogen::OrderSQLService;

use Turbogen::SQLService;

sub save_order {

    my $order = shift;

    my $db = Turbogen::SQLService::connect();

    $order->{customer_id} = get_customer_id($db, $order->{email});

    unless ($order->{customer_id}) {

        $order->{customer_id} = create_customer($db, $order);
    }

    my $order_id = create_order($db, $order);

    Turbogen::SQLService::disconnect($db);

    return $order_id;
}

sub get_customer_id {

    my $db = shift;
    my $email = shift;

    my $query = get_customer_id_query();

    my $results = $db->selectrow_arrayref($query, undef, $email);

    return $results->[0];
}

sub get_customer_id_query {

    return <<SQL;
        SELECT ID
        FROM Turbogen.Customers
        WHERE Email = ?
SQL
}

sub create_customer {

    my $db = shift;
    my $order = shift;

    my $query = create_customer_query();

    my @values = (
        $order->{email},
        $order->{name}
    );

    $db->do($query, undef, @values) or die($db->errstr());

    return $db->last_insert_id(undef, 'Turbogen', 'Customers', 'ID');
}

sub create_customer_query {

    return <<SQL;
        INSERT INTO Turbogen.Customers(Email, Name)
        VALUES(?, ?)
SQL
}

sub create_order {

    my $db = shift;
    my $order = shift;

    my $query = create_order_query();

    my @values = (
        $order->{customer_id},
        $order->{vehicle}->{id},
        $order->{trim_level}->{id},
        $order->{quantity}
    );

    $db->do($query, undef, @values) or die($db->errstr());

    return $db->last_insert_id(undef, 'Turbogen', 'Orders', 'ID');
}

sub create_order_query {

    return <<SQL;
        INSERT INTO Turbogen.Orders(CustomerID, VehicleID, TrimLevelID, Quantity)
        VALUES(?, ?, ?, ?)
SQL
}

return 1;
