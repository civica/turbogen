package Turbogen::Prompt;

use Turbogen::OrderSQLService;
use Turbogen::VehicleSQLService;

sub get_order {

    my $order;
    my $is_valid = 0;

    until ($is_valid) {

        $order->{name} = get_full_name();
        $order->{email} = get_email();
        $order->{vehicle} = get_vehicle();

        unless (%{$order->{vehicle}}) {

            return $order;
        }

        $order->{trim_level} = get_trim_level($order->{vehicle});
        $order->{quantity} = get_quantity();

        $is_valid = confirm_order($order);
    }

    $order_id = Turbogen::OrderSQLService::save_order($order);

    print_order_confirmation($order_id);

    return $order;
}

sub get_full_name {

    my $name;
    my $is_valid = 0;

    until ($is_valid) {

        print("Please enter your full name: ");

        chomp($name = <STDIN>);

        $is_valid = validate_name($name);

        unless ($is_valid) {

            print("Invalid name.\n");
        }
    }

    return $name;
}

sub validate_name {

    my $name = shift;

    return $name =~ /^[a-z-'. ]+$/i;
}

sub get_email {

    my $email;
    my $is_valid = 0;

    until ($is_valid) {

        print("Please enter your email address (\@hotmail.co.uk / \@hotmail.com only): ");

        chomp($email = <STDIN>);

        $is_valid = validate_email($email);

        unless ($is_valid) {

            print("Invalid email address.\n");
        }
    }

    return $email;
}

sub validate_email {

    my $email = shift;

    return $email =~ /^[\w.+-]+\@hotmail\.co(?:\.uk|m)$/i;
}

sub get_vehicle {

    my $available_vehicles = Turbogen::VehicleSQLService::get_available_vehicles();

    print_available_vehicles($available_vehicles);

    unless (scalar(@$available_vehicles)) {

        return;
    }

    my $vehicle;

    until ($vehicle) {

        print("Please enter the vehicle you would like to order: ");

        chomp(my $keyword = <STDIN>);

        $vehicle = get_hash_by_keyword($available_vehicles, $keyword);

        unless (%$vehicle) {

            print("Invalid vehicle.\n");
        }
    }

    return $vehicle;
}

sub print_available_vehicles {

    my $available_vehicles = shift;

    unless (scalar(@$available_vehicles)) {

        print("There are no vehicles available.\n");

        return;
    }

    print("The following vehicles are available:\n");

    foreach my $available_vehicle (@$available_vehicles) {

        print("- $available_vehicle->{name} ($available_vehicle->{keyword})\n");
    }
}

sub get_hash_by_keyword {

    my $hashes = shift;
    my $keyword = shift;

    foreach my $hash (@$hashes) {

        if ($keyword =~ /^$hash->{keyword}$/i) {

            return $hash;
        }
    }
}

sub get_trim_level {

    my $vehicle = shift;

    unless (%$vehicle) {

        return;
    }

    my $available_trim_levels = $vehicle->{trim_levels};

    print_available_trim_levels($available_trim_levels);

    unless (scalar(@$available_trim_levels)) {

        return;
    }

    my $trim_level;

    until ($trim_level) {

        print("Please enter the trim level you would like to order: ");

        chomp(my $keyword = <STDIN>);

        $trim_level = get_hash_by_keyword($available_trim_levels, $keyword);

        unless (%$trim_level) {

            print("Invalid trim level.\n");
        }
    }

    return $trim_level;
}

sub print_available_trim_levels {

    my $trim_levels = shift;

    unless (scalar(@$trim_levels)) {

        print("There are no trim levels available.\n");

        return;
    }

    print("The following trim levels are available:\n");

    foreach my $trim_level (@$trim_levels) {

        print("- $trim_level->{name} ($trim_level->{keyword})\n");
    }
}

sub get_quantity {

    my $quantity;
    my $is_valid = 0;

    until ($is_valid) {

        print("Please enter the number of vehicles you would like to order: ");

        chomp($quantity = <STDIN>);

        $is_valid = validate_quantity($quantity);

        unless ($is_valid) {

            print("Invalid quantity.\n");
        }
    }

    return $quantity;
}

sub validate_quantity {

    my $quantity = shift;

    return $quantity > 0;
}

sub confirm_order {

    my $order = shift;

    my $order_summary = get_order_summary($order);

    print("You entered:\n");
    print("$order_summary\n");
    print("Is this correct? (y/N): ");

    chomp(my $confirmation = <STDIN>);

    return validate_confirmation($confirmation);
}

sub get_order_summary {

    my $order = shift;

    my $name = $order->{name} || '(Unknown)';
    my $email = $order->{email} || '(Unknown)';

    $summary = "Name $name, Email $email";

    my $vehicle = $order->{vehicle}->{name};
    my $quantity = $order->{quantity};

    unless ($vehicle && $quantity) {

        return "$summary has not ordered a vehicle";
    }

    $summary .= " has ordered $vehicle";

    my $trim_level = $order->{trim_level}->{name};

    if ($trim_level) {

        $summary .= " with $trim_level trim";
    }

    $summary .= " x $quantity";

    return $summary;
}

sub validate_confirmation {

    my $confirmation = shift;

    return $confirmation =~ /^y$/i;
}

sub print_order_confirmation {

    my $order_id = shift;

    print "Order $order_id has been submitted.\n";
}

return 1;
