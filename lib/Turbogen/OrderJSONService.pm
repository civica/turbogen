package Turbogen::OrderJSONService;

use File::Path;
use File::Spec;

use Turbogen::FileService;
use Turbogen::JSONService;
use Turbogen::TimeService;

sub save_order {

    my $order = shift;

    my $file_name = get_new_json_file_name();

    my $json = Turbogen::JSONService::encode_hash_to_json($order);

    return save_json_to_file($file_name, $json);
}

sub get_new_json_file_name {

    my $timestamp = Turbogen::TimeService::get_current_timestamp();

    return "$timestamp.json";
}

sub save_json_to_file {

    my $file_name = shift;
    my $json = shift;

    my $directory_path = create_order_directory();

    my $file_path = File::Spec->catfile($directory_path, $file_name);

    Turbogen::FileService::write_scalar_to_file($file_path, $json);

    return $file_path;
}

sub create_order_directory {

    my $directory_path = 'orders';

    File::Path::make_path($directory_path);

    return $directory_path;
}

return 1;
