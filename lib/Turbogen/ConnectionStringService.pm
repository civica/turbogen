package Turbogen::ConnectionStringService;

use Turbogen::FileService;
use Turbogen::JSONService;

sub get_connection_string {

    my $name = shift;

    my $json = Turbogen::FileService::read_file_to_scalar('ConnectionStrings.json');

    my $connection_strings = Turbogen::JSONService::decode_json_to_hash($json);

    return $connection_strings->{$name};
}

return 1;
