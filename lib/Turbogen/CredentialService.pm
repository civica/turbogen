package Turbogen::CredentialService;

use Turbogen::FileService;
use Turbogen::JSONService;

sub get_credential {

    my $name = shift;

    my $json = Turbogen::FileService::read_file_to_scalar('Credentials.json');

    my $credentials = Turbogen::JSONService::decode_json_to_hash($json);

    return $credentials->{$name};
}

return 1;
