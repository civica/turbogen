package Turbogen::SQLService;

use DBI;

use Turbogen::ConnectionStringService;
use Turbogen::CredentialService;

sub connect {

    my $connection_string = Turbogen::ConnectionStringService::get_connection_string('turbogen');

    my $credential = Turbogen::CredentialService::get_credential('turbogen');

    my $username = $credential->{username};
    my $password = $credential->{password};

    return DBI->connect($connection_string, $username, $password, { RaiseError => 1 }) or die($DBI::errstr);
}

sub disconnect {

    my $db = shift;

    $db->disconnect() or die($db->errstr());
}

return 1;
