package Turbogen::FileService;

sub read_file_to_scalar {

    my $file_path = shift;

    $array = read_file_to_array($file_path);

    return join('', @$array);
}

sub read_file_to_array {

    my $file_path = shift;

    open(my $file, '<', $file_path) or die("Can't open $file_path: $!");

    my @contents = <$file>;

    close($file) or die("Can't close $file_path: $!");

    return \@contents;
}

sub write_scalar_to_file {

    my $file_path = shift;
    my $contents = shift;

    open(my $file, '>', $file_path) or die("Can't open $file_path: $!");

    print($file $contents);

    close($file) or die("Can't close $file_path: $!");
}

return 1;
