package Turbogen::VehicleSQLService;

use Turbogen::SQLService;

sub get_available_vehicles {

    my $db = Turbogen::SQLService::connect();

    my $results = select_all_vehicles($db);

    Turbogen::SQLService::disconnect($db);

    return convert_vehicle_results_to_hash($results);
}

sub select_all_vehicles {

    my $db = shift;

    my $query = select_all_vehicles_query();

    return $db->selectall_arrayref($query, { Slice => {} });
}

sub select_all_vehicles_query {

    return <<SQL;
        SELECT
            Vehicles.ID AS vehicle_id,
            Vehicles.Keyword AS vehicle_keyword,
            Vehicles.Name AS vehicle_name,
            TrimLevels.ID AS trimlevel_id,
            TrimLevels.Keyword AS trimlevel_keyword,
            TrimLevels.Name AS trimlevel_name,
            TrimLevels.VehicleID AS trimlevel_vehicleid
        FROM Turbogen.TrimLevels
        LEFT JOIN Turbogen.Vehicles
        ON Vehicles.ID = TrimLevels.VehicleID
        ORDER BY Vehicles.ID, TrimLevels.ID
SQL
}

sub convert_vehicle_results_to_hash {

    my $results = shift;

    my $vehicles = [];

    my $current_vehicle;

    foreach my $row (@$results) {

        if ($row->{vehicle_id} != $current_vehicle->{id}) {

            $current_vehicle = {};

            $current_vehicle->{id} = $row->{vehicle_id};
            $current_vehicle->{keyword} = $row->{vehicle_keyword};
            $current_vehicle->{name} = $row->{vehicle_name};

            $current_vehicle->{trim_levels} = [];

            push($vehicles, $current_vehicle);
        }

        my $current_trim_level = {};

        $current_trim_level->{id} = $row->{trimlevel_id};
        $current_trim_level->{name} = $row->{trimlevel_name};
        $current_trim_level->{keyword} = $row->{trimlevel_keyword};
        $current_trim_level->{vehicle_id} = $row->{trimlevel_vehicleid};

        push($current_vehicle->{trim_levels}, $current_trim_level);
    }

    return $vehicles;
}

return 1;
