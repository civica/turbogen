package Turbogen::VehicleXMLService;

use XML::LibXML;

sub get_available_vehicles {

    my $file_name = shift || 'Vehicles.xml';

    my $document = load_xml_document($file_name);

    my $vehicles = [];

    foreach my $vehicle_node ($document->findnodes('/Vehicles/Vehicle')) {

        my $vehicle = {};

        $vehicle->{keyword} = $vehicle_node->findvalue('./Keyword');
        $vehicle->{name} = $vehicle_node->findvalue('./Name');
        $vehicle->{trim_levels} = [];

        foreach my $trim_level_node ($vehicle_node->findnodes('./TrimLevels/TrimLevel')) {

            $trim_level = {};

            $trim_level->{keyword} = $trim_level_node->findvalue('./Keyword');
            $trim_level->{name} = $trim_level_node->findvalue('./Name');

            push($vehicle->{trim_levels}, $trim_level);
        }

        push($vehicles, $vehicle);
    }

    return $vehicles;
}

sub load_xml_document {

    my $file_name = shift;

    return XML::LibXML->load_xml(location => $file_name);
}

return 1;
