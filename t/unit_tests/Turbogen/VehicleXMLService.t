#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::MockModule;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::VehicleXMLService;

subtest get_available_vehicles => sub {

    my $mock_service = Test::MockModule->new('Turbogen::VehicleXMLService');

    my @cases = (
        {
            scenario => 'When an XML file containing an array of vehicles is loaded Then the contents match',
            expected => [
                {
                    keyword => 'e',
                    name => 'The Elephant',
                    trim_levels => [
                        {
                            keyword => 'af',
                            name => 'African'
                        },
                        {
                            keyword => 'as',
                            name => 'Asian'
                        },
                        {
                            keyword => 'b',
                            name => 'Borneo'
                        },
                        {
                            keyword => 'f',
                            name => 'Forest'
                        }
                    ]
                },
                {
                    keyword => 'm',
                    name => 'The Mongoose',
                    trim_levels => [
                        {
                            keyword => 'm',
                            name => 'Meerkat'
                        },
                        {
                            keyword => 'b',
                            name => 'Bandit'
                        },
                        {
                            keyword => 'h',
                            name => 'Helogale'
                        }
                    ]
                },
                {
                    keyword => 'w',
                    name => 'The Warthog',
                    trim_levels => [
                        {
                            keyword => 'c',
                            name => 'Common'
                        },
                        {
                            keyword => 'd',
                            name => 'Desert'
                        },
                        {
                            keyword => 'l',
                            name => 'LesserSpotted'
                        }
                    ]
                }
            ],

            mock_xml => qq|<?xml version="1.0" encoding="UTF-8"?>
                <Vehicles>
                    <Vehicle>
                        <Keyword>e</Keyword>
                        <Name>The Elephant</Name>
                        <TrimLevels>
                            <TrimLevel>
                                <Keyword>af</Keyword>
                                <Name>African</Name>
                            </TrimLevel>
                            <TrimLevel>
                                <Keyword>as</Keyword>
                                <Name>Asian</Name>
                            </TrimLevel>
                            <TrimLevel>
                                <Keyword>b</Keyword>
                                <Name>Borneo</Name>
                            </TrimLevel>
                            <TrimLevel>
                                <Keyword>f</Keyword>
                                <Name>Forest</Name>
                            </TrimLevel>
                        </TrimLevels>
                    </Vehicle>
                    <Vehicle>
                        <Keyword>m</Keyword>
                        <Name>The Mongoose</Name>
                        <TrimLevels>
                            <TrimLevel>
                                <Keyword>m</Keyword>
                                <Name>Meerkat</Name>
                            </TrimLevel>
                            <TrimLevel>
                                <Keyword>b</Keyword>
                                <Name>Bandit</Name>
                            </TrimLevel>
                            <TrimLevel>
                                <Keyword>h</Keyword>
                                <Name>Helogale</Name>
                            </TrimLevel>
                        </TrimLevels>
                    </Vehicle>
                    <Vehicle>
                        <Keyword>w</Keyword>
                        <Name>The Warthog</Name>
                        <TrimLevels>
                            <TrimLevel>
                                <Keyword>c</Keyword>
                                <Name>Common</Name>
                            </TrimLevel>
                            <TrimLevel>
                                <Keyword>d</Keyword>
                                <Name>Desert</Name>
                            </TrimLevel>
                            <TrimLevel>
                                <Keyword>l</Keyword>
                                <Name>LesserSpotted</Name>
                            </TrimLevel>
                        </TrimLevels>
                    </Vehicle>
                </Vehicles>|
        },
        {
            scenario => 'When an empty XML file is loaded Then no vehicles are recorded',
            expected => [],

            mock_xml => '<Vehicles></Vehicles>'
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 2;

            $mock_service->mock('load_xml_document', sub {

                my $file_name = shift;

                is($file_name, 'Vehicles.xml', 'Correct file name supplied');

                return XML::LibXML->load_xml(string => $case->{mock_xml});
            });

            my $actual = Turbogen::VehicleXMLService::get_available_vehicles();

            is_deeply($actual, $case->{expected}, 'XML matches');
        };
    }
};
