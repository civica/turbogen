#!/usr/bin/perl

use strict;
use warnings;

use DBD::Mock;
use FindBin;
use Test::MockModule;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::VehicleSQLService;

subtest get_available_vehicles => sub {

    plan tests => 3;

    my $mock_sql_service = Test::MockModule->new('Turbogen::SQLService');

    $mock_sql_service->mock('connect', sub {

        my $db = DBI->connect('DBI:Mock:', undef, undef, { RaiseError => 1 }) or die($DBI::errstr);

        $db->{mock_session} = DBD::Mock::Session->new('get_available_vehicles' => (
            {
                statement => Turbogen::VehicleSQLService::select_all_vehicles_query(),

                results => [
                    [
                        'vehicle_id',
                        'vehicle_keyword',
                        'vehicle_name',
                        'trimlevel_id',
                        'trimlevel_keyword',
                        'trimlevel_name',
                        'trimlevel_vehicleid'
                    ],
                    [
                        '132',
                        'e',
                        'The Elephant',
                        '481',
                        'af',
                        'African',
                        '132'
                    ],
                    [
                        '132',
                        'e',
                        'The Elephant',
                        '490',
                        'as',
                        'Asian',
                        '132'
                    ],
                    [
                        '132',
                        'e',
                        'The Elephant',
                        '491',
                        'b',
                        'Borneo',
                        '132'
                    ],
                    [
                        '132',
                        'e',
                        'The Elephant',
                        '801',
                        'f',
                        'Forest',
                        '132'
                    ],
                    [
                        '207',
                        'm',
                        'The Mongoose',
                        '560',
                        'm',
                        'Meerkat',
                        '207'
                    ],
                    [
                        '207',
                        'm',
                        'The Mongoose',
                        '579',
                        'b',
                        'Bandit',
                        '207'
                    ],
                    [
                        '207',
                        'm',
                        'The Mongoose',
                        '582',
                        'h',
                        'Helogale',
                        '207'
                    ],
                    [
                        '349',
                        'w',
                        'The Warthog',
                        '670',
                        'c',
                        'Common',
                        '349'
                    ],
                    [
                        '349',
                        'w',
                        'The Warthog',
                        '675',
                        'd',
                        'Desert',
                        '349'
                    ],
                    [
                        '349',
                        'w',
                        'The Warthog',
                        '698',
                        'l',
                        'LesserSpotted',
                        '349'
                    ]
                ]
            }
        ));

        pass('Connected to database');

        return $db;
    });

    $mock_sql_service->mock('disconnect', sub {

        pass('Disconnected from database');
    });

    my $expected = [
        {
            id => 132,
            keyword => 'e',
            name => 'The Elephant',
            trim_levels => [
                {
                    id => 481,
                    keyword => 'af',
                    name => 'African',
                    vehicle_id => 132
                },
                {
                    id => 490,
                    keyword => 'as',
                    name => 'Asian',
                    vehicle_id => 132
                },
                {
                    id => 491,
                    keyword => 'b',
                    name => 'Borneo',
                    vehicle_id => 132,
                },
                {
                    id => 801,
                    keyword => 'f',
                    name => 'Forest',
                    vehicle_id => 132
                }
            ]
        },
        {
            id => 207,
            keyword => 'm',
            name => 'The Mongoose',
            trim_levels => [
                {
                    id => 560,
                    keyword => 'm',
                    name => 'Meerkat',
                    vehicle_id => 207
                },
                {
                    id => 579,
                    keyword => 'b',
                    name => 'Bandit',
                    vehicle_id => 207
                },
                {
                    id => 582,
                    keyword => 'h',
                    name => 'Helogale',
                    vehicle_id => 207
                }
            ]
        },
        {
            id => 349,
            keyword => 'w',
            name => 'The Warthog',
            trim_levels => [
                {
                    id => 670,
                    keyword => 'c',
                    name => 'Common',
                    vehicle_id => 349
                },
                {
                    id => 675,
                    keyword => 'd',
                    name => 'Desert',
                    vehicle_id => 349
                },
                {
                    id => 698,
                    keyword => 'l',
                    name => 'LesserSpotted',
                    vehicle_id => 349
                }
            ]
        }
    ];

    my $actual = Turbogen::VehicleSQLService::get_available_vehicles();

    is_deeply($actual, $expected, 'Results match');
};
