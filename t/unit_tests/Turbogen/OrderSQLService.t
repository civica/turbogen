#!/usr/bin/perl

use strict;
use warnings;

use DBD::Mock;
use FindBin;
use Test::MockModule;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::OrderSQLService;
use Turbogen::SQLService;
use Turbogen::TimeService;
use Turbogen::VehicleSQLService;

subtest save_order => sub {

    my $mock_sql_service = Test::MockModule->new('Turbogen::SQLService');

    my @cases = (
        {
            scenario => 'When an order is supplied for an existing customer Then the saved order details are correct',
            expected => 1, # Order ID - first record inserted in session

            order => {
                email => 'test@hotmail.com',
                name => 'Testy McTestface',
                vehicle => {
                    id => 101
                },
                trim_level => {
                    id => 202
                },
                quantity => 4
            },

            mock_session => DBD::Mock::Session->new('save_order' => (
                {
                    statement => Turbogen::OrderSQLService::get_customer_id_query(),

                    bound_params => [ 'test@hotmail.com' ], # Email

                    results => [
                        [ 'ID' ],
                        [ 1298 ]
                    ]
                },
                {
                    statement => Turbogen::OrderSQLService::create_order_query(),

                    bound_params => [
                        '1298', # Customer ID
                        '101', # Vehicle ID
                        '202', # Trim Level ID
                        '4' # Quantity
                    ],

                    results => [
                        [ 'rows' ],
                        []
                    ]
                }
            ))
        },
        {
            scenario => 'When an order is supplied for a new customer Then the saved order details are correct',
            expected => 2, # Order ID - second inserted in session

            order => {
                email => 'new_customer@hotmail.com',
                name => 'Conor Summer',
                vehicle => {
                    id => 303
                },
                trim_level => {
                    id => 404
                },
                quantity => 7
            },

            mock_session => DBD::Mock::Session->new('save_order' => (
                {
                    statement => Turbogen::OrderSQLService::get_customer_id_query(),

                    bound_params => [ 'new_customer@hotmail.com' ], # Email

                    results => [
                        [ 'ID' ]
                    ]
                },
                {
                    statement => Turbogen::OrderSQLService::create_customer_query(),

                    bound_params => [
                        'new_customer@hotmail.com', # Email
                        'Conor Summer' # Name
                    ],

                    results => [
                        [ 'rows' ],
                        []
                    ]
                },
                {
                    statement => Turbogen::OrderSQLService::create_order_query(),

                    bound_params => [
                        '1', # Customer ID - first record inserted in session
                        '303', # Vehicle ID
                        '404', # Trim Level ID
                        '7' # Quantity
                    ],

                    results => [
                        [ 'rows' ],
                        []
                    ]
                }
            ))
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 3;

            $mock_sql_service->mock('connect', sub {

                my $db = DBI->connect('DBI:Mock:', undef, undef, { RaiseError => 1 }) or die($DBI::errstr);

                $db->{mock_session} = $case->{mock_session};

                pass('Connected to database');

                return $db;
            });

            $mock_sql_service->mock('disconnect', sub {

                pass('Disconnected from database');
            });

            my $actual = Turbogen::OrderSQLService::save_order($case->{order});

            is($actual, $case->{expected}, 'Order created and assigned ID');
        };
    }
};
