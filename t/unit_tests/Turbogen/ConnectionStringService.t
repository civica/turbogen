#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::MockModule;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::ConnectionStringService;

subtest get_connection_string => sub {

    my $mock_file_service = Test::MockModule->new('Turbogen::FileService');
    my $mock_json_service = Test::MockModule->new('Turbogen::JSONService');

    my @cases = (
        {
            scenario => 'When turbogen is supplied Then the matching connection string is returned',
            expected => 'mock connection string',

            name => 'turbogen',

            mock_json => '{ "turbogen": "mock connection string" }',
            mock_hash => {
                turbogen => 'mock connection string'
            }
        },
        {
            scenario => 'When an invalid name is supplied Then a connection string is not returned',
            expected => undef,

            name => 'invalid',

            mock_json => '{ "valid": "valid connection string" }',
            mock_hash => {
                valid => 'valid connection string'
            }
        },
        {
            scenario => 'When the source file is missing Then a connection string is not returned',
            expected => undef,

            name => 'valid'
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 3; 

            $mock_file_service->mock('read_file_to_scalar', sub {

                my $name = shift;

                is($name, 'ConnectionStrings.json', 'Correct file name supplied');

                return $case->{mock_json};
            });

            $mock_json_service->mock('decode_json_to_hash', sub {

                my $json = shift;

                is($json, $case->{mock_json}, 'Correct JSON supplied');

                return $case->{mock_hash};
            });

            my $actual = Turbogen::ConnectionStringService::get_connection_string($case->{name});

            is($actual, $case->{expected}, 'Correct connection string returned');
        };
    }
};
