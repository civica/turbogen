#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::More tests => 6;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::Prompt;

subtest get_hash_by_keyword => sub {

    my @cases = (
        {
            scenario => 'When a matching keyword and case is supplied Then a hash is returned',
            expected => 'a',

            hashes => [
                {
                    keyword => 'a'
                }
            ],
            keyword => 'a'
        },
        {
            scenario => 'When a matching keyword but different case is supplied Then a hash is returned',
            expected => 'DEF',

            hashes => [
                {
                    keyword => 'ABC'
                },
                {
                    keyword => 'DEF'
                },
                {
                    keyword => 'GHI'
                }
            ],
            keyword => 'dEf'
        },
        {
            scenario => 'When a matching keyword is not supplied Then a hash is not returned',
            expected => undef,

            hashes => [
                {
                    keyword => 'a'
                }
            ],
            keyword => 'b'
        },
        {
            scenario => 'When there are no hashes Then a hash is not returned',
            expected => undef,

            hashes => [],
            keyword => 'a'
        },
        {
            scenario => 'When the hash reference is undefined Then a hash is not returned',
            expected => undef,

            keyword => 'a'
        },
        {
            scenario => 'When all references are undefined Then a hash is not returned',
            expected => undef
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $hash = Turbogen::Prompt::get_hash_by_keyword($case->{hashes}, $case->{keyword});

            my $actual = $hash ? $hash->{keyword} : undef;

            is($actual, $case->{expected});
        };
    }
};

subtest get_order_summary => sub {

    my @cases = (
        {
            scenario => 'When The Warthog is selected Then all order details are correct',
            expected => 'Name John Doe, Email john.doe@hotmail.com has ordered The Warthog with Desert trim x 3',

            email => 'john.doe@hotmail.com',
            name => 'John Doe',
            quantity => 3,
            trim_level => {
                name => 'Desert'
            },
            vehicle => {
                name => 'The Warthog'
            }
        },
        {
            scenario => 'When The Elephant is selected Then all order details are correct',
            expected => 'Name Florian Cloud de Bounevialle O\'Malley Armstrong, Email dido@gmail.com has ordered The Elephant with Asian trim x 12',

            email => 'dido@gmail.com',
            name => 'Florian Cloud de Bounevialle O\'Malley Armstrong',
            quantity => 12,
            trim_level => {
                name => 'Asian'
            },
            vehicle => {
                name => 'The Elephant'
            }
        },
        {
            scenario => 'When The Mongoose is selected Then all order details are correct',
            expected => 'Name Nick, Email n287@outlook.co.uk has ordered The Mongoose with Bandit trim x 1',

            email => 'n287@outlook.co.uk',
            name => 'Nick',
            quantity => 1,
            trim_level => {
                name => 'Bandit'
            },
            vehicle => {
                name => 'The Mongoose'
            }
        },
        {
            scenario => 'When a quantity is omitted Then the vehicle is excluded from the order summary',
            expected => 'Name Ali Baba, Email ali@ba.ba has not ordered a vehicle',

            email => 'ali@ba.ba',
            name => 'Ali Baba',
            quantity => '',
            trim_level => {
                name => 'Borneo'
            },
            vehicle => {
                name => 'The Elephant'
            }
        },
        {
            scenario => 'When a trim level is omitted Then the trim level is excluded from the order summary',
            expected => 'Name Al Dente, Email al@yahoo.com has ordered The Warthog x 5312',

            email => 'al@yahoo.com',
            name => 'Al Dente',
            quantity => 5312,
            trim_level => {},
            vehicle => {
                name => 'The Warthog'
            }
        },
        {
            scenario => 'When a vehicle is omitted Then the vehicle is excluded from the order summary',
            expected => 'Name Donna Kebab, Email d.kebab@company.com has not ordered a vehicle',

            email => 'd.kebab@company.com',
            name => 'Donna Kebab',
            quantity => 8,
            trim_level => {
                name => 'Common'
            },
            vehicle => {}
        },
        {
            scenario => 'When the order details are blank Then all details are excluded from the order summary',
            expected => 'Name (Unknown), Email (Unknown) has not ordered a vehicle',

            email => '',
            name => '',
            quantity => '',
            trim_level => {
                name => ''
            },
            vehicle => {
                name => ''
            }
        },
        {
            scenario => 'When the order is not supplied Then all details are excluded from the order summary',
            expected => 'Name (Unknown), Email (Unknown) has not ordered a vehicle'
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $actual = Turbogen::Prompt::get_order_summary($case);

            is($actual, $case->{expected});
        };
    }
};

subtest validate_confirmation => sub {

    my @cases = (
        {
            scenario => 'When lowercase y is supplied Then true is returned',
            expected => 1,

            confirmation => 'y'
        },
        {
            scenario => 'When uppercase Y is supplied Then true is returned',
            expected => 1,

            confirmation => 'Y'
        },
        {
            scenario => 'When n is supplied Then false is returned',
            expected => '',

            confirmation => 'n'
        },
        {
            scenario => 'When y is supplied with spaces Then false is returned',
            expected => '',

            confirmation => ' y'
        },
        {
            scenario => 'When yes is supplied Then false is returned',
            expected => '',

            confirmation => 'yes'
        },
        {
            scenario => 'When no is supplied Then false is returned',
            expected => '',

            confirmation => 'no'
        },
        {
            scenario => 'When no value is supplied Then false is returned',
            expected => ''
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $actual = Turbogen::Prompt::validate_confirmation($case->{confirmation});

            is($actual, $case->{expected});
        };
    }
};

subtest validate_email => sub {

    my @cases = (
        {
            scenario => 'When a hotmail.com email is supplied Then true is returned',
            expected => 1,

            email => 'john.doe@hotmail.com'
        },
        {
            scenario => 'When a hotmail.co.uk email is supplied Then true is returned',
            expected => 1,

            email => 'jane.dough@hotmail.co.uk',
        },
        {
            scenario => 'When a hotmail.com email is supplied with mixed casing Then true is returned',
            expected => 1,

            email => 'doe.DOUGH_do-do+123@HOTmail.CoM'
        },
        {
            scenario => 'When a gmail.com email is supplied Then false is returned',
            expected => '',

            email => 'dido@gmail.com'
        },
        {
            scenario => 'When a hotmail email is supplied without a fully-qualified domain Then false is returned',
            expected => '',

            email => 'al.dente@hotmail'
        },
        {
            scenario => 'When a hotmail.uk email is supplied Then false is returned',
            expected => '',

            email => 'al.dente@hotmail.uk'
        },
        {
            scenario => 'When a blank hotmail.com email is supplied Then false is returned',
            expected => '',

            email => '@hotmail.com'
        },
        {
            scenario => 'When a blank email is supplied Then false is returned',
            expected => '',

            email => ''
        },
        {
            scenario => 'When no email is supplied Then false is returned',
            expected => ''
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $actual = Turbogen::Prompt::validate_email($case->{email});

            is($actual, $case->{expected});
        };
    }
};

subtest validate_name => sub {

    my @cases = (
        {
            scenario => 'When a forename and surname are supplied Then true is returned',
            expected => 1,

            name => 'John Doe'
        },
        {
            scenario => 'When a long name is supplied Then true is returned',
            expected => 1,

            name => 'Florian Cloud de Bounevialle O\'Malley Armstrong',
        },
        {
            scenario => 'When a single name is supplied Then true is returned',
            expected => 1,

            name => 'Nick'
        },
        {
            scenario => 'When a name with dots, apostrophes, and dashes is supplied Then true is returned',
            expected => 1,

            name => 'H. O\'Pot-Amous'
        },
        {
            scenario => 'When a name with numbers is supplied Then false is returned',
            expected => '',

            name => 'John Doe the 1st'
        },
        {
            scenario => 'When a name with brackets is supplied Then false is returned',
            expected => '',

            name => 'Terrence (Terry) C. Orange'
        },
        {
            scenario => 'When a blank name is supplied Then false is returned',
            expected => '',

            name => ''
        },
        {
            scenario => 'When a name is not supplied Then false is returned',
            expected => ''
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $actual = Turbogen::Prompt::validate_name($case->{name});

            is($actual, $case->{expected});
        };
    }
};

subtest validate_quantity => sub {

    my @cases = (
        {
            scenario => 'When 1 is supplied Then true is returned',
            expected => 1,

            quantity => '1'
        },
        {
            scenario => 'When a large positive number is supplied Then true is returned',
            expected => 1,

            quantity => '28745'
        },
        {
            scenario => 'When 0 is supplied Then false is returned',
            expected => '',

            quantity => '0'
        },
        {
            scenario => 'When a negative number is supplied Then false is returned',
            expected => '',

            quantity => '-1'
        },
        {
            scenario => 'When letters are supplied Then false is returned',
            expected => '',

            quantity => 'one'
        },
        {
            scenario => 'When no value is supplied Then false is returned',
            expected => ''
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $actual = Turbogen::Prompt::validate_quantity($case->{quantity});

            is($actual, $case->{expected});
        };
    }
};
