#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::MockModule;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::FileService;
use Turbogen::OrderJSONService;

subtest save_order => sub {

    my $mock_file_service = Test::MockModule->new('Turbogen::FileService');
    my $mock_file_path_service = Test::MockModule->new('File::Path');
    my $mock_file_spec_service = Test::MockModule->new('File::Spec');
    my $mock_json_service = Test::MockModule->new('Turbogen::JSONService');
    my $mock_time_service = Test::MockModule->new('Turbogen::TimeService');

    my @cases = (
        {
            scenario => 'When an order is encoded to JSON Then the contents match',
            expected => 'orders/20200129-101542.json',

            order => {
                email => 'al.berta@hotmail.co.uk',
                name => 'Al Berta',
                quantity => '16',
                trim_level => 'Asian',
                vehicle => 'The Elephant'
            },

            mock_timestamp => '20200129-101542',
            mock_json => qq|
                {
                    "email": "al.berta\@hotmail.co.uk",
                    "name": "Al Berta",
                    "quantity": "16",
                    "trim_level": "Asian",
                    "vehicle": "The Elephant"
                }|
        },
        {
            scenario => 'When an empty order is encoded to JSON Then no order details are recorded',
            expected => 'orders/20200129-103027.json',

            order => {},

            mock_timestamp => '20200129-103027',
            mock_json => '{}'
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 6;

            $mock_time_service->mock('get_current_timestamp', sub {

                pass('Current timestamp returned');

                return $case->{mock_timestamp};
            });

            $mock_json_service->mock('encode_hash_to_json', sub {

                my $hash = shift;

                is_deeply($hash, $case->{order}, 'Correct order encoded');

                return $case->{mock_json};
            });

            $mock_file_path_service->mock('make_path', sub {

                my $directory_path = shift;

                is($directory_path, 'orders', 'Correct directory path created');
            });

            $mock_file_service->mock('write_scalar_to_file', sub {

                my $file_path = shift;
                my $json = shift;

                is($file_path, $case->{expected}, 'Correct file path supplied');
                is($json, $case->{mock_json}, 'Correct JSON supplied');
            });

            my $actual = Turbogen::OrderJSONService::save_order($case->{order});

            is_deeply($actual, $case->{expected}, 'Correct file path returned');
        };
    }
};
