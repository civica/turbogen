#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::MockTime;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::TimeService;

subtest get_current_timestamp => sub {

    my @cases = (
        {
            scenario => 'When the current time is supplied Then a formatted timestamp is returned',
            expected => '20201227-031508',

            time => '2020-12-27T03:15:08Z'
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1; 

            Test::MockTime::set_fixed_time($case->{time});

            my $actual = Turbogen::TimeService::get_current_timestamp();

            is($actual, $case->{expected});
        };
    }
};
