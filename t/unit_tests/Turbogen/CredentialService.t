#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::MockModule;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::CredentialService;

subtest get_credential => sub {

    my $mock_file_service = Test::MockModule->new('Turbogen::FileService');
    my $mock_json_service = Test::MockModule->new('Turbogen::JSONService');

    my @cases = (
        {
            scenario => 'When turbogen is supplied Then the matching credential is returned',
            expected => {
                username => 'turbogen_admin',
                password => 'Turbogen Password 123'
            },

            name => 'turbogen',

            mock_json => '{ "turbogen": { "username": "turbogen admin", "password": "Turbogen Password 123" } }',
            mock_hash => {
                turbogen => {
                    username => 'turbogen_admin',
                    password => 'Turbogen Password 123'
                }
            }
        },
        {
            scenario => 'When an invalid name is supplied Then a credential is not returned',
            expected => undef,

            name => 'invalid',

            mock_json => '{ "valid": { "username": "dog", "password": "edom" } }',
            mock_hash => {
                valid => {
                    username => 'dog',
                    password => 'edom'
                }
            }
        },
        {
            scenario => 'When the source file is missing Then a credential is not returned',
            expected => undef,

            name => 'valid'
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 3;

            $mock_file_service->mock('read_file_to_scalar', sub {

                my $name = shift;

                is($name, 'Credentials.json', 'Correct file name supplied');

                return $case->{mock_json};
            });

            $mock_json_service->mock('decode_json_to_hash', sub {

                my $json = shift;

                is($json, $case->{mock_json}, 'Correct JSON supplied');

                return $case->{mock_hash};
            });

            my $actual = Turbogen::CredentialService::get_credential($case->{name});

            is_deeply($actual, $case->{expected}, 'Correct credential returned');
        };
    }
};
