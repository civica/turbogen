#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::CredentialService;

subtest get_credential => sub {

    my @cases = (
        {
            scenario => 'When turbogen is supplied Then the matching credential is returned',
            expected => {
                username => 'turbogen_admin',
                password => 'Turbogen Password 123'
            },

            name => 'turbogen'
        },
        {
            scenario => 'When an invalid name is supplied Then a credential is not returned',
            expected => undef,

            name => 'invalid'
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $actual = Turbogen::CredentialService::get_credential($case->{name});

            is_deeply($actual, $case->{expected});
        };
    }
};
