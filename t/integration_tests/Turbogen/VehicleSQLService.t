#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::VehicleSQLService;

subtest get_available_vehicles => sub {

    my $expected = [
        {
            name => 'The Elephant',
            keyword => 'e',

            trim_levels => [
                {
                    name => 'African',
                    keyword => 'af'
                },
                {
                    name => 'Asian',
                    keyword => 'as'
                },
                {
                    name => 'Borneo',
                    keyword => 'b'
                },
                {
                    name => 'Forest',
                    keyword => 'f'
                }
            ]
        },
        {
            name => 'The Mongoose',
            keyword => 'm',

            trim_levels => [
                {
                    name => 'Meerkat',
                    keyword => 'm'
                },
                {
                    name => 'Bandit',
                    keyword => 'b'
                },
                {
                    name => 'Helogale',
                    keyword => 'h'
                }
            ]
        },
        {
            name => 'The Warthog',
            keyword => 'w',

            trim_levels => [
                {
                    name => 'Common',
                    keyword => 'c'
                },
                {
                    name => 'Desert',
                    keyword => 'd'
                },
                {
                    name => 'LesserSpotted',
                    keyword => 'l'
                }
            ]
        }
    ];

    plan tests => scalar @$expected;

    my $actual = Turbogen::VehicleSQLService::get_available_vehicles();

    foreach my $vehicle_index (0..$#$expected) {

        my $expected_vehicle = $expected->[$vehicle_index];
        my $actual_vehicle = $actual->[$vehicle_index];

        subtest $expected_vehicle->{name} => sub {

            plan tests => 3 + scalar @{$expected_vehicle->{trim_levels}};

            assert_vehicle($expected_vehicle, $actual_vehicle);

            foreach my $trim_level_index (0..$#{$expected_vehicle->{trim_levels}}) {

                my $expected_trim_level = $expected_vehicle->{trim_levels}->[$trim_level_index];
                my $actual_trim_level = $actual_vehicle->{trim_levels}->[$trim_level_index];

                subtest $expected_trim_level->{name} => sub {

                    plan tests => 4;

                    assert_trim_level($expected_trim_level, $actual_trim_level, $actual_vehicle->{id});
                };
            }
        };
    }
};

sub assert_vehicle {

    my $expected = shift;
    my $actual = shift;

    my $name = $expected->{name};

    ok($actual->{id} > 0, "$name vehicle ID is set");
    is($actual->{name}, $expected->{name}, "$name name is correct");
    is($actual->{keyword}, $expected->{keyword}, "$name keyword is correct");
}

sub assert_trim_level {

    my $expected = shift;
    my $actual = shift;
    my $actual_vehicle_id = shift;

    my $name = $expected->{name};

    ok($actual->{id} > 0, "$name trim level ID is set");
    is($actual->{name}, $expected->{name}, "$name name is correct");
    is($actual->{keyword}, $expected->{keyword}, "$name keyword is correct");
    is($actual->{vehicle_id}, $actual_vehicle_id, "$name vehicle ID matches vehicle");
}
