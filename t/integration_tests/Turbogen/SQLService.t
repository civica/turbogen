#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::SQLService;

subtest connect => sub {

    plan tests => 3;

    my $db = Turbogen::SQLService::connect();

    isnt($db, undef, 'A database handle is returned');

    is($db->ping(), 1, 'The database handle is connected');

    Turbogen::SQLService::disconnect($db);

    is($db->ping(), '', 'The database handle is disconnected');
};
