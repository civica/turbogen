#!/usr/bin/perl

use strict;
use warnings;

use File::Spec;
use FindBin;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::FileService;
use Turbogen::OrderJSONService;

subtest save_order => sub {

    my @cases = (
        {
            scenario => 'When an order is encoded to JSON Then the contents match',
            file_name => '1.json',

            order => {
                email => 'al.berta@hotmail.co.uk',
                name => 'Al Berta',
                quantity => '16',
                trim_level => 'Asian',
                vehicle => 'The Elephant'
            }
        },
        {
            scenario => 'When an empty order is encoded to JSON Then no order details are recorded',
            file_name => '2.json',

            order => {}
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $expected_file_path = File::Spec->catfile($FindBin::Bin, 'OrderJSONService', $case->{file_name});

            my $expected = Turbogen::FileService::read_file_to_array($expected_file_path);

            my $actual_file_path = Turbogen::OrderJSONService::save_order($case->{order});

            my $actual = Turbogen::FileService::read_file_to_array($actual_file_path);

            is_deeply($actual, $expected);
        };
    }
};
