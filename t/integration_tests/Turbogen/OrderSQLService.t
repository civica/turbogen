#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::OrderSQLService;
use Turbogen::SQLService;
use Turbogen::TimeService;
use Turbogen::VehicleSQLService;

subtest save_order => sub {

    my $available_vehicles = Turbogen::VehicleSQLService::get_available_vehicles();

    my $timestamp = Turbogen::TimeService::get_current_timestamp();

    my @cases = (
        {
            scenario => 'When an order is supplied for an existing customer Then the saved order details are correct',

            order => {
                email => 'test@hotmail.com',
                name => 'Testy McTestface',
                vehicle => $available_vehicles->[0],
                trim_level => $available_vehicles->[0]->{trim_levels}->[0],
                quantity => 4
            }
        },
        {
            scenario => 'When an order is supplied for a new customer Then the saved order details are correct',

            order => {
                email => "new_customer_$timestamp\@hotmail.com",
                name => 'Conor Summer',
                vehicle => $available_vehicles->[1],
                trim_level => $available_vehicles->[1]->{trim_levels}->[2],
                quantity => 7
            }
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 28;

            my $order_id = Turbogen::OrderSQLService::save_order($case->{order});

            ok($order_id > 0, 'Order created and assigned ID');

            my $actual = select_order($order_id);

            assert_order($case->{order}, $order_id);
        };
    }
};

sub assert_order {

    my $expected = shift;
    my $order_id = shift;

    my $actual = select_order($order_id);

    is($actual->{order_id}, $order_id, "Order ID matches");
    ok($actual->{order_customerid} > 0, "Order customer ID is set");
    ok($actual->{order_vehicleid} > 0, "Order vehicle ID is set");
    ok($actual->{order_trimlevelid} > 0, "Order trim level ID is set");
    is($actual->{order_quantity}, $expected->{quantity}, "Order quantity matches");
    isnt($actual->{order_created}, undef, "Order created set");
    is($actual->{order_updated}, undef, "Order updated not set");
    is($actual->{order_isdeleted}, undef, "Order is deleted not set");

    is($actual->{customer_id}, $expected->{customer_id}, "Customer ID matches");
    is($actual->{customer_email}, $expected->{email}, "Customer email matches");
    is($actual->{customer_name}, $expected->{name}, "Customer name matches");
    isnt($actual->{customer_created}, undef, "Customer created set");
    is($actual->{customer_updated}, undef, "Customer updated not set");
    is($actual->{customer_isdeleted}, undef, "Customer is deleted not set");

    is($actual->{vehicle_id}, $expected->{vehicle}->{id}, "Vehicle ID matches");
    is($actual->{vehicle_name}, $expected->{vehicle}->{name}, "Vehicle name matches");
    is($actual->{vehicle_keyword}, $expected->{vehicle}->{keyword}, "Vehicle keyword matches");
    isnt($actual->{vehicle_created}, undef, "Vehicle created set");
    is($actual->{vehicle_updated}, undef, "Vehicle updated not set");
    is($actual->{vehicle_isdeleted}, undef, "Vehicle is deleted not set");

    is($actual->{trimlevel_id}, $expected->{trim_level}->{id}, "Trim Level ID matches");
    is($actual->{trimlevel_vehicleid}, $expected->{vehicle}->{id}, "Trim Level vehicle ID matches");
    is($actual->{trimlevel_name}, $expected->{trim_level}->{name}, "Trim Level name matches");
    is($actual->{trimlevel_keyword}, $expected->{trim_level}->{keyword}, "Trim Level keyword matches");
    isnt($actual->{trimlevel_created}, undef, "Trim Level created set");
    is($actual->{trimlevel_updated}, undef, "Trim Level updated not set");
    is($actual->{trimlevel_isdeleted}, undef, "Trim Level is deleted not set");
}

sub select_order {

    my $db = Turbogen::SQLService::connect();

    my $order_id = shift;

    my $query = <<SQL;
        SELECT
            Orders.ID AS 'order_id',
            Orders.CustomerID AS 'order_customerid',
            Orders.VehicleID AS 'order_vehicleid',
            Orders.TrimLevelID AS 'order_trimlevelid',
            Orders.Quantity AS 'order_quantity',
            Orders.Created AS 'order_created',
            Orders.Updated AS 'order_updated',
            Orders.IsDeleted AS 'order_isdeleted',

            Customers.ID AS 'customer_id',
            Customers.Email AS 'customer_email',
            Customers.Name AS 'customer_name',
            Customers.Created AS 'customer_created',
            Customers.Updated AS 'customer_updated',
            Customers.IsDeleted AS 'customer_isdeleted',

            Vehicles.ID AS 'vehicle_id',
            Vehicles.Name AS 'vehicle_name',
            Vehicles.Keyword AS 'vehicle_keyword',
            Vehicles.Created AS 'vehicle_created',
            Vehicles.Updated AS 'vehicle_updated',
            Vehicles.IsDeleted AS 'vehicle_isdeleted',

            TrimLevels.ID AS 'trimlevel_id',
            TrimLevels.VehicleID AS 'trimlevel_vehicleid',
            TrimLevels.Name AS 'trimlevel_name',
            TrimLevels.Keyword AS 'trimlevel_keyword',
            TrimLevels.Created AS 'trimlevel_created',
            TrimLevels.Updated AS 'trimlevel_updated',
            TrimLevels.IsDeleted AS 'trimlevel_isdeleted'

        FROM Turbogen.Orders

        LEFT JOIN Turbogen.Customers ON Customers.ID = Orders.CustomerID
        LEFT JOIN Turbogen.Vehicles ON Vehicles.ID = Orders.VehicleID
        LEFT JOIN Turbogen.TrimLevels ON TrimLevels.ID = Orders.TrimLevelID

        WHERE Orders.ID = ?
SQL

    my $result = $db->selectrow_hashref($query, undef, $order_id);

    Turbogen::SQLService::disconnect($db);

    return $result;
}
