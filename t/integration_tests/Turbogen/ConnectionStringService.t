#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::ConnectionStringService;

subtest get_connection_string => sub {

    my @cases = (
        {
            scenario => 'When turbogen is supplied Then the matching connection string is returned',
            expected => 'dbi:mysql:database=Turbogen;host=localhost',

            name => 'turbogen'
        },
        {
            scenario => 'When an invalid name is supplied Then a connection string is not returned',
            expected => undef,

            name => 'invalid'
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $actual = Turbogen::ConnectionStringService::get_connection_string($case->{name});

            is($actual, $case->{expected});
        };
    }
};
