#!/usr/bin/perl

use strict;
use warnings;

use File::Spec;
use FindBin;
use Test::More tests => 1;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::VehicleXMLService;

subtest get_available_vehicles => sub {

    my @cases = (
        {
            scenario => 'When an XML file containing an array of vehicles is loaded Then the contents match',
            expected => [
                {
                    keyword => 'e',
                    name => 'The Elephant',
                    trim_levels => [
                        {
                            keyword => 'af',
                            name => 'African'
                        },
                        {
                            keyword => 'as',
                            name => 'Asian'
                        },
                        {
                            keyword => 'b',
                            name => 'Borneo'
                        },
                        {
                            keyword => 'f',
                            name => 'Forest'
                        }
                    ]
                },
                {
                    keyword => 'm',
                    name => 'The Mongoose',
                    trim_levels => [
                        {
                            keyword => 'm',
                            name => 'Meerkat'
                        },
                        {
                            keyword => 'b',
                            name => 'Bandit'
                        },
                        {
                            keyword => 'h',
                            name => 'Helogale'
                        }
                    ]
                },
                {
                    keyword => 'w',
                    name => 'The Warthog',
                    trim_levels => [
                        {
                            keyword => 'c',
                            name => 'Common'
                        },
                        {
                            keyword => 'd',
                            name => 'Desert'
                        },
                        {
                            keyword => 'l',
                            name => 'LesserSpotted'
                        }
                    ]
                }
            ],

            file_name => '1.xml'
        },
        {
            scenario => 'When an empty XML file is loaded Then no vehicles are recorded',
            expected => [],

            file_name => '2.xml'
        }
    );

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $file_path = File::Spec->catfile($FindBin::Bin, 'VehicleXMLService', $case->{file_name});

            my $actual = Turbogen::VehicleXMLService::get_available_vehicles($file_path);

            is_deeply($actual, $case->{expected});
        };
    }
};
