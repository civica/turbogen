#!/usr/bin/perl

use strict;
use warnings;

use File::Spec;
use File::Temp;
use FindBin;
use Test::More tests => 3;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::FileService;
use Turbogen::TimeService;

my @array_cases = (
    {
        scenario => 'When a JSON file is loaded into or saved from an array Then the contents should match',
        file_name => '1.json',

        contents => [
            qq|{\n|,
            qq|    "Calvous": "Tambour",\n|,
            qq|    "Grandisonant": [\n|,
            qq|        "Macrotone",\n|,
            qq|        "Chromolithography"\n|,
            qq|    ],\n|,
            qq|    "Vert": 5249,\n|,
            qq|    "Knobber": {\n|,
            qq|        "Hexaemeron": {\n|,
            qq|            "Ballyhoo": "Ruffianize"\n|,
            qq|        }\n|,
            qq|    }\n|,
            qq|}\n|
        ]
    },
    {
        scenario => 'When an XML file is loaded into or saved from an array Then the contents should match',
        file_name => '2.xml',

        contents => [
            qq|<Minatory>\n|,
            qq|    <Aerobe>\n|,
            qq|        <Menald>Pulverous</Menald>\n|,
            qq|        <Invaginate>4923482</Invaginate>\n|,
            qq|    </Aerobe>\n|,
            qq|    <Statoscope>Jade</Statoscope>\n|,
            qq|    <Hexastichal>false</Hexastichal>\n|,
            qq|    <Pseudography>\n|,
            qq|        <Kyriolexy>\n|,
            qq|            <Plethysmography>Breccia</Plethysmography>\n|,
            qq|        </Kyriolexy>\n|,
            qq|    </Pseudography>\n|,
            qq|</Minatory>\n|
        ]
    },
    {
        scenario => 'When an empty text file is loaded into or saved from an array Then the contents are empty',
        file_name => '3.txt',

        contents => []
    }
);

my @scalar_cases = (
    {
        scenario => 'When a JSON file is loaded into or saved from a scalar Then the contents should match',
        file_name => '1.json',

        contents => <<JSON
{
    "Calvous": "Tambour",
    "Grandisonant": [
        "Macrotone",
        "Chromolithography"
    ],
    "Vert": 5249,
    "Knobber": {
        "Hexaemeron": {
            "Ballyhoo": "Ruffianize"
        }
    }
}
JSON
    },
    {
        scenario => 'When an XML file is loaded into or saved from a scalar Then the contents should match',
        file_name => '2.xml',

        contents => <<XML
<Minatory>
    <Aerobe>
        <Menald>Pulverous</Menald>
        <Invaginate>4923482</Invaginate>
    </Aerobe>
    <Statoscope>Jade</Statoscope>
    <Hexastichal>false</Hexastichal>
    <Pseudography>
        <Kyriolexy>
            <Plethysmography>Breccia</Plethysmography>
        </Kyriolexy>
    </Pseudography>
</Minatory>
XML
    },
    {
        scenario => 'When an empty text file is loaded into or saved from a scalar Then the contents are empty',
        file_name => '3.txt',

        contents => ''
    }
);

subtest read_file_to_array => sub {

    plan tests => scalar @array_cases;

    foreach my $case (@array_cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $file_path = File::Spec->catfile($FindBin::Bin, 'FileService', $case->{file_name});

            my $actual = Turbogen::FileService::read_file_to_array($file_path);

            is_deeply($actual, $case->{contents});
        };
    }
};

subtest read_file_to_scalar => sub {

    plan tests => scalar @scalar_cases;

    foreach my $case (@scalar_cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $file_path = File::Spec->catfile($FindBin::Bin, 'FileService', $case->{file_name});

            my $actual = Turbogen::FileService::read_file_to_scalar($file_path);

            is($actual, $case->{contents});
        };
    }
};

subtest write_scalar_to_file => sub {

    plan tests => scalar @scalar_cases;

    foreach my $case (@scalar_cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $expected_file_path = File::Spec->catfile($FindBin::Bin, 'FileService', $case->{file_name});

            my $expected = Turbogen::FileService::read_file_to_array($expected_file_path);

            my $timestamp = Turbogen::TimeService::get_current_timestamp();

            my $actual_file_path = File::Temp->new();

            Turbogen::FileService::write_scalar_to_file($actual_file_path, $case->{contents});

            my $actual = Turbogen::FileService::read_file_to_array($actual_file_path);

            is_deeply($actual, $expected);
        };
    }
};
