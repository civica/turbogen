#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use Test::More tests => 2;

use lib "$FindBin::Bin/../../../lib";

use Turbogen::JSONService;

my @cases = (
    {
        scenario => 'When a hash is encoded to or decoded from JSON Then the contents match',

        json => <<JSON,
{
   "Calvous" : "Tambour",
   "Grandisonant" : [
      "Macrotone",
      "Chromolithography"
   ],
   "Knobber" : {
      "Hexaemeron" : {
         "Ballyhoo" : "Ruffianize"
      }
   },
   "Vert" : 5249
}
JSON
        hash => {
            Calvous => "Tambour",
            Grandisonant => [
                "Macrotone",
                "Chromolithography"
            ],
            Vert => 5249,
            Knobber => {
                Hexaemeron => {
                    Ballyhoo => "Ruffianize"
                }
            }
        }
    },
    {
        scenario => 'When an empty hash is encoded to or decoded from JSON Then the contents are empty',

        json => "{}\n",
        hash => {}
    }
);

subtest decode_json_to_hash => sub {

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $actual = Turbogen::JSONService::decode_json_to_hash($case->{json});

            is_deeply($actual, $case->{hash});
        };
    }
};

subtest encode_hash_to_json => sub {

    plan tests => scalar @cases;

    foreach my $case (@cases) {

        subtest $case->{scenario} => sub {

            plan tests => 1;

            my $actual = Turbogen::JSONService::encode_hash_to_json($case->{hash});

            is($actual, $case->{json});
        };
    }
};
