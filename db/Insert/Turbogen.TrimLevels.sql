SET @TheElephantID = (SELECT ID FROM Turbogen.Vehicles WHERE Keyword = 'e');

INSERT INTO Turbogen.TrimLevels
(VehicleID, Name, Keyword)
VALUES
(@TheElephantID, 'African', 'af'),
(@TheElephantID, 'Asian', 'as'),
(@TheElephantID, 'Borneo', 'b'),
(@TheElephantID, 'Forest', 'f');

SET @TheMongooseID = (SELECT ID FROM Turbogen.Vehicles WHERE Keyword = 'm');

INSERT INTO Turbogen.TrimLevels
(VehicleID, Name, Keyword)
VALUES
(@TheMongooseID, 'Meerkat', 'm'),
(@TheMongooseID, 'Bandit', 'b'),
(@TheMongooseID, 'Helogale', 'h');

SET @TheWarthogID = (SELECT ID FROM Turbogen.Vehicles WHERE Keyword = 'w');

INSERT INTO Turbogen.TrimLevels
(VehicleID, Name, Keyword)
VALUES
(@TheWarthogID, 'Common', 'c'),
(@TheWarthogID, 'Desert', 'd'),
(@TheWarthogID, 'LesserSpotted', 'l');
