# Turbogen

## Setup

### CPAN

```
sudo cpanm DBD::Mock Test::MockModule
```

### MySQL

- Execute the following scripts from the `db` folder to create and seed the *Turbogen* database:
    - Create
        - Turbogen.sql
        - Turbogen.Vehicles.sql
        - Turbogen.TrimLevels.sql
        - Turbogen.Customers.sql
        - Turbogen.Orders.sql
    - Insert
        - Turbogen.Vehicles.sql
        - Turbogen.TrimLevels.sql
        - Turbogen.Customers.sql
- Create a MySQL user account:
    - Open `Credentials.json`.
    - Change the username and password for the *turbogen* credential.
    - Create a new matching user account in MySQL which has permission to perform CRUD operations on the Turbogen database.

## Running the program

```
perl main.pl
```

## Running the tests

```
prove -r
```